use actix::Addr;
use actix_files as fs;
use actix_files::NamedFile;
use actix_web::middleware::Logger;
use actix_web::{get, web, App, HttpServer, Responder};

use crate::handlers::new_game;
use crate::runtime::GameServer;

use crate::tic_tac_toe::{TttGame, TttWish};

pub async fn run_server(
    listener: Addr<GameServer<TttWish>>,
) -> std::io::Result<()> {
    env_logger::init();

    log::info!("starting server");
    let game_listener = web::Data::new(listener);
    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(game_listener.clone())
            .service(index)
            .service(
                web::resource("/api/tic_tac_toe/new_game/{user_id}/{wish}")
                    .to(new_game::<TttGame>),
            )
            .service(fs::Files::new("/static", "./static"))
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}

#[get("/")]
pub async fn index() -> impl Responder {
    log::info!("index");
    NamedFile::open("./index.html")
}
