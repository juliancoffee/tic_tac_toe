mod core;
mod domain;
mod handlers;
mod lobby;
mod observers;
mod runtime;
mod server;
mod tic_tac_toe;

use actix::Actor;
use tic_tac_toe::TttWish;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {

    let game_server = runtime::GameServer::<TttWish>::default().start();

    server::run_server(game_server).await
}
